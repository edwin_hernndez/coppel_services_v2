const hapi = require('hapi')
const Inert = require('inert');
const mongoose = require('mongoose')
const MongoDBUrl = 'mongodb://localhost:27017/coppel'
const Path = require('path');
const JWT         = require('jsonwebtoken');
const userModel = require('./models/user');
const secret = 'behwqbwjhqbehjebwq';

const server = hapi.server({
    host: '192.241.244.243',
    port: 3000,
    routes: {
        files: {
            relativeTo: Path.join(__dirname, 'public')
        }
    }
})

const validate = async function (decoded, request) {
    console.log('************************');
    console.log(decoded);

    let user = await userModel.findOne({
        mail: decoded.mail
    },(err, res) => {});


    if(!user){
        console.log("false")
        return { isValid: false };
    }else{
        console.log("true")
        return { isValid: true };
    }
};


const provision = async () => {

    await server.register(Inert);

    await server.start();

    await server.register(require('hapi-auth-jwt2'));

    server.auth.strategy('jwt', 'jwt',
    {   
        key: secret,          // Never Share your secret key
        validate: validate,            // validate function defined above
        verifyOptions: { algorithms: [ 'HS256' ] } // pick a strong algorithm
    });

    server.auth.default('jwt'); // Pone la autenticación a todas las rutas

    server.route(
        [].concat(
    
            require('./routes/users').endpoints(),
            require('./routes/articulos').endpoints(),
            require('./routes/pedidos').endpoints(),
            require('./routes/auth').endpoints()
        )
    );

    mongoose.connect(MongoDBUrl, {
        useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false }).then(() => { 
            console.log(`Connected to Mongo server `, MongoDBUrl) }, err => { console.log(err) });
   
    console.log("server runing alv", server.info.uri)
};

provision();