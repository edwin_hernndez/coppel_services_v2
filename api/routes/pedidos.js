const pedidosController = require('../controllers/pedidos')
const Joi = require('joi')

exports.endpoints = () => {
    let endpoints = [
        {
            method: 'DELETE',
            path: '/deletePedido',
            options: {validate: {payload: Joi.object({
                idPedido: Joi.string().required(),
            }),
            failAction: (request, h, err)  => {
                if (err.isJoi && Array.isArray(err.details) && err.details.length > 0) {
                    const invalidItem = err.details[0];
                    return h.response({statusCode: 400,
                                       error: "Bad Request" ,
                                       message: err.details[0].message}).code(400).takeover();
                }
                return h.response(err).takeover()
              }
            }},
            handler: pedidosController.deletePedido
        },
        {
            method: 'PUT',
            path: '/updatePedido',
            options: {validate: {payload: Joi.object({
                dirEntrega: Joi.string().min(5).max(50).required(),
                latAndLong: Joi.string().min(5).max(50).required(),
                FechaEntrega: Joi.string().min(5).max(50).required(),
                CompradorName: Joi.string().min(5).max(50).required(),
                idPedido: Joi.string().min(5).max(50).required()
            }),
            failAction: (request, h, err)  => {
                if (err.isJoi && Array.isArray(err.details) && err.details.length > 0) {
                    const invalidItem = err.details[0];
                    return h.response({statusCode: 400,
                                       error: "Bad Request" ,
                                       message: err.details[0].message}).code(400).takeover();
                }
                return h.response(err).takeover()
              }
        }},
            handler: pedidosController.updatePedido
        },
        {
            method: 'GET',
            path: '/getAllPedidos/{idUser}',
            handler: pedidosController.getAllPedidos

        },
        {
            method: 'POST',
            path: '/createPedido',
            options: {validate: {payload: Joi.object({
                dirEntrega: Joi.string().min(5).max(50).required(),
                latAndLong: Joi.string().min(5).max(50).required(),
                FechaEntrega: Joi.string().min(5).max(50).required(),
                CompradorName: Joi.string().min(5).max(50).required(),
                idUser: Joi.string().required(),
                numberOfArts: Joi.string().required()
            }),
            failAction: (request, h, err)  => {
                if (err.isJoi && Array.isArray(err.details) && err.details.length > 0) {
                    const invalidItem = err.details[0];
                    return h.response({statusCode: 400,
                                       error: "Bad Request" ,
                                       message: err.details[0].message}).code(400).takeover();
                }
                return h.response(err).takeover()
              }
        }},
            handler: pedidosController.createPedido
        }
    ]
    return endpoints
}