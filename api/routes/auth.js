const userController = require('../controllers/auth')
const Joi = require('joi')

exports.endpoints = () => {
    let endpoints = [
        {
                method: 'POST',
                path: '/login',
                config: {
                    auth: false, //Configuración de autenticación
                    validate: {
                        payload: Joi.object({
                            mail: Joi.string().email().required(),
                            password: Joi.string().required(),
                    }),
                    failAction: (request, h, err)  => {
                        if (err.isJoi && Array.isArray(err.details) && err.details.length > 0) {
                            const invalidItem = err.details[0];
                            return h.response({statusCode: 400,
                                            error: "Bad Request" ,
                                            message: err.details[0].message}).code(400).takeover();
                        }
                        return h.response(err).takeover()
                    }
            }},
            handler: userController.login
        }
    ]
    return endpoints
}