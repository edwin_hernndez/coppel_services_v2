const userController = require('../controllers/users')
const Joi = require('joi')

exports.endpoints = () => {

    let endpoints = [
        {
            method: 'POST',
            path: '/registerUser',
            config: 
            {
                auth: false, //Configuración de autenticación
                validate: {payload: Joi.object({
                mail: Joi.string().email().required(),
                name: Joi.string().min(5).max(50).required(),
                lastName: Joi.string().min(5).max(50).required(),
                //password: Joi.string().regex(/^(?=.[a-z])(?=.[A-Z])(?=.[0-9])(?=.[!@#\$%\^&\*-_])(?=.{8,})/).required(),
                password: Joi.string().min(5).max(50).required()
            }),
            failAction: (request, h, err)  => {
                if (err.isJoi && Array.isArray(err.details) && err.details.length > 0) {
                    const invalidItem = err.details[0];
                    return h.response({statusCode: 400,
                                       error: "Bad Request" ,
                                       message: err.details[0].message}).code(400).takeover();
                }
                return h.response(err).takeover()
              }
        }},
            handler: userController.createUser
        },{
            method: 'DELETE',
            path: '/deleteUser',
            options: {validate: {payload: Joi.object({
                mail: Joi.string().email().required(),
                password: Joi.string().required(),
            }),
            failAction: (request, h, err)  => {
                if (err.isJoi && Array.isArray(err.details) && err.details.length > 0) {
                    const invalidItem = err.details[0];
                    return h.response({statusCode: 400,
                                       error: "Bad Request" ,
                                       message: err.details[0].message}).code(400).takeover();
                }
                return h.response(err).takeover()
              }
        }},
            handler: userController.deleteUser
        }
    ]
    return endpoints
}