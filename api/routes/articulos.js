const articulosController = require('../controllers/articulos')
const Joi = require('joi')
const Inert = require('inert')
const fs = require('fs')
const Path = require('path');
const moment = require('moment');

const handleFileUpload = file => {
    return new Promise((resolve, reject) => {
      const filename = moment().format("X")+file.hapi.filename
      const data = file._data
      
      var folder = Path.join(__dirname+'/public', filename);
  
      fs.writeFile(`./public/${filename}`, data, err => {
        if (err) {
          reject(err)
        }
        resolve({
          message: 'Imagen subida exitosamente',
          imageUrl: `http://192.241.244.243:3000/${filename}`
        })
      })
    })
}
  

exports.endpoints = () => {
    let endpoints = [
        {
          method: 'DELETE',
          path: '/deleteArticulo',
          options: {validate: {payload: Joi.object({
            idArticulo: Joi.string().required(),
        }),
        failAction: (request, h, err)  => {
            if (err.isJoi && Array.isArray(err.details) && err.details.length > 0) {
                const invalidItem = err.details[0];
                return h.response({statusCode: 400,
                                   error: "Bad Request" ,
                                   message: err.details[0].message}).code(400).takeover();
            }
            return h.response(err).takeover()
          }
        }},
          handler: articulosController.deleteArticulo  
        },
        {
            method: 'PUT',
            path: '/editArticulo',
            options: {validate: {payload: Joi.object({
                name: Joi.string().min(5).max(50).required(),
                image: Joi.string().min(5).max(50).required(),
                price: Joi.string().min(5).max(50).required(),
                description: Joi.string().min(5).max(50).required(),
                idArticulo: Joi.string().required()
            }),
            failAction: (request, h, err)  => {
                if (err.isJoi && Array.isArray(err.details) && err.details.length > 0) {
                    const invalidItem = err.details[0];
                    return h.response({statusCode: 400,
                                       error: "Bad Request" ,
                                       message: err.details[0].message}).code(400).takeover();
                }
                return h.response(err).takeover()
              }
        }},
            handler: articulosController.editArticulo
        },
        {
            method: 'GET',
            path: '/getAllArticulos',
            handler: articulosController.getAllArticulos
        },
        {
            method: 'GET',
            path: '/{param*}',
            config: {
              auth: false //Configuración de autenticación
            },
            handler: {
                directory: {
                    path: '.',
                    redirectToSlash: true,
                    index: true,
                }
            }
        },
        {
            method: 'POST',
            path: '/uploadImage',
            options: {
                payload: {
                  output: 'stream'
                }
              },
              handler: async (req, h) => {
                const { payload } = req
          
                return handleFileUpload(payload.file)
              }
        },
        {
            method: 'GET',
            path:'/getArticulosByUser/{idUser}',
            handler: articulosController.getArticuloByUser
        },{
            method: 'POST',
            path: '/nuevoArticulo',
            options: {validate: {payload: Joi.object({
                name: Joi.string().min(5).max(50).required(),
                image: Joi.string().min(5).max(150).required(),
                price: Joi.string().min(5).max(50).required(),
                description: Joi.string().min(5).max(50).required(),
                idUser: Joi.string().required()
            }),
            failAction: (request, h, err)  => {
                if (err.isJoi && Array.isArray(err.details) && err.details.length > 0) {
                    const invalidItem = err.details[0];
                    return h.response({statusCode: 400,
                                       error: "Bad Request" ,
                                       message: err.details[0].message}).code(400).takeover();
                }
                return h.response(err).takeover()
              }
        }},
            handler: articulosController.createArticulo
        }
    ]
    return endpoints
}