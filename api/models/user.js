const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userModel = new Schema({
  mail: { type: String, required: true, index: { unique: true } },
  name: { type: String, required: true },
  lastName: {type: String, requiered: true},
  password: { type: String, required: true },
});

module.exports = mongoose.model('User', userModel, 'users');