const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const pedidosModel = new Schema({
  dirEntrega: { type: String, required: true, index: { unique: true } },
  latAndLong: { type: String, required: true },
  FechaEntrega: {type: String, requiered: true},
  CompradorName: { type: String, required: true },
  idUser: {type: String, required: true}
});

module.exports = mongoose.model('Pedidos', pedidosModel, 'pedidos');