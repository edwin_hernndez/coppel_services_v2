const articuloModel = require('../models/articulos')

exports.createArticulo = (req, h) => {
    const articulo = { 
        name: req.payload.name,
        image: req.payload.image,
        price: req.payload.price,
        description: req.payload.description,
        idUser: req.payload.idUser
    }

    return new Promise(resolve => {
        articuloModel.create(articulo).then( (articulo, error) => {
            if (error){
                resolve({message: "Error al crear el articulo"})
            }
            resolve({ message: "Articulo creado exitosamente", articulo: articulo});
        }).catch((err) => {

            resolve({message: "Error al crear el articulo", err: err});
        });
    })
}

exports.getArticuloByUser = (req, h) => {
    return new Promise(resolve => {
        articuloModel.find({
            idUser: req.params.idUser
        },
        (err, res) => {
            if(!res){
                resolve({
                    mesage: "Error al obtener los articulos"
                })
            }
            resolve(res)
        })
    })
}


exports.getAllArticulos = (req, h) => {
    return new Promise(resolve => {
        articuloModel.find(
        (err, res) => {
            if(!res){
                resolve({
                    mesage: "Error al obtener los articulos"
                })
            }
            resolve(res)
        })
    })
}

exports.editArticulo = (req, h) => {
    return new Promise(resolve => {
        articuloModel.findByIdAndUpdate(req.payload.idArticulo,{
            name: req.payload.name,
            price: req.payload.price,
            image: req.payload.image,
            description: req.payload.description

        }, {new:true},(err,res) => {
            if (!res){
                resolve({message: "No hay no existe con ese id"})
            }
            resolve({ message: "Articulo actualizado exitosamente", articulo: res});
        })
    })
}

exports.deleteArticulo = (req, h) => {
    return new Promise(resolve => {
        articuloModel.findByIdAndDelete(req.payload.idArticulo, {
            idArticulo: req.payload.idArticulo
        },
        (err, res) => {
            if(!res){
                resolve({
                    message: "Articulo no econtrado"
                })
            }
            resolve({
                message: "Articulo eliminado exitosamente", res
            })
        })
    })
}