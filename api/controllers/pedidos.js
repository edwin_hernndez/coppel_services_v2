const pedidoModel = require("../models/pedidos")

exports.createPedido = (req, h) => {
    const pedido = {
        dirEntrega: req.payload.dirEntrega,
        latAndLong: req.payload.latAndLong,
        FechaEntrega: req.payload.FechaEntrega,
        CompradorName: req.payload.CompradorName,
        idUser: req.payload.idUser,
        numberOfArts: req.payload.numberOfArts
    }

    return new Promise( resolve => {
        pedidoModel.create(pedido).then((pedido, error) => {
            if(error){
                resolve({message: "Error"})
            }
            resolve({ message: "Pedido registrado exitosamente", pedido: pedido});
        })
    })
}

exports.getAllPedidos = (req, h) => {
    return new Promise(resolve => {
        pedidoModel.find({
            idUser: req.params.idUser
        }, 
            (err, res) => {
                if(!res) {
                    resolve({
                        message: "Error al obtener los pedidos"
                    })
                }
                resolve(res)
            }
        )
    })
}

exports.updatePedido = (req, h) => {
    return new Promise(resolve => {
        pedidoModel.findByIdAndUpdate(req.payload.idPedido, {
            dirEntrega: req.payload.dirEntrega,
            latAndLong: req.payload.latAndLong,
            FechaEntrega: req.payload.FechaEntrega,
            CompradorName: req.payload.CompradorName
        }, {new:true}, (err, res) => {
            if (!res){
                resolve({message: "No existe el Pedido"})
            }
            resolve({ message: "Pedido actualizado exitosamente", pedido: res});
        })
    })
}

exports.deletePedido = (req, h) => {
    return new Promise(resolve => {
        pedidoModel.findByIdAndDelete(req.payload.idPedido, {
            idPedido: req.payload.idPedido
        },
        (err, res) => {
            if(!res){
                resolve({
                    message: "Pedido no econtrado"
                })
            }
            resolve({
                message: "Pedido eliminado exitosamente", res
            })
        })
    })
}