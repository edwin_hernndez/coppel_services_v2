const userModel = require('../models/user')

exports.createUser = (req, h) => {
    const user = { 
        mail: req.payload.mail,
        name: req.payload.name,
        lastName: req.payload.lastName,
        password: req.payload.password
    }

    return new Promise(resolve => {
        userModel.create(user).then( (user, error) => {

            if (error){
                resolve({message: "Error"})
            }
            resolve({
                 message: "Usuario creado exitosamente.", 
                 userName: user.name,
                lastName: user.lastName });
        }).catch((err) => {

            if (err.code === 11000) {
                resolve({message: "El usuario ya existe"});
            }

            resolve({message: "El usuario ya existe", err: err.code});
        });
    })
}

exports.deleteUser = (req, h) => {
    return new Promise(resolve => {
        userModel.deleteOne({
            mail: req.payload.mail,
            password: req.payload.password
        }, 
         (err, res) => {
            if(!res){
                resolve({
                    mesage: "Usuario o contraseña incorrecta"
                })
            }
            resolve(res)
        })
    })
}